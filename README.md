# Quasar App (desafio-tecnico-cima)

Desafio Tecnico CIMA | Clon de Spotify

## Clona el repositorio

<br />

Haz click en el boton azul que dice clonar, y copia el link HTTPS

![Gitlab](https://res.cloudinary.com/dpnv2uar8/image/upload/v1674265759/Screenshot_2023-01-20_194759_gnb1mw.jpg)

Despues en tu terminal escribe lo siguiente:

```bash
git clone <EL LINK QUE COPIASTE>
```

Despues navega hacia el folder que se descargo y abrelo en tu IDE preferido

## Instalar las dependencias

Ahora a instalar las dependencias!

```bash
yarn
# or
npm install
```

### Inicia el servidor para desarrollo

Ya instaladas las dependencias, solo debes de correr este comando

```bash
quasar dev
```

### Construye la aplicacion para produccion

<br />

```bash
quasar build
```
